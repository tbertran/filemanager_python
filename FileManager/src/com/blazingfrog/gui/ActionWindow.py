'''
Created on Oct 9, 2010

@author: thomas
'''
from wx import wx
class ActionWindow(wx.Dialog):
    retCode = ""
    def __init__(self, parent, id, title, action):
        wx.Dialog.__init__(self, parent, id, title, size=(145, 175))
        sizer =  self.CreateTextSizer("     Select an Action\n")
        actionBtn = wx.Button(self, -1, action)
        archiveBtn = wx.Button(self, -1, 'Archive', size=(135,50))
        cancelBtn = wx.Button(self, -1, 'Cancel')
        sizer.Add(actionBtn, 0, wx.ALL|wx.ALIGN_CENTER|wx.EXPAND, 5)
        sizer.Add(archiveBtn, 0, wx.ALL|wx.ALIGN_CENTER|wx.EXPAND, 5)
        sizer.Add(cancelBtn, 0, wx.ALL|wx.ALIGN_CENTER|wx.EXPAND, 5)
        
        actionBtn.Bind(wx.EVT_BUTTON, self.ActionBtn)
        archiveBtn.Bind(wx.EVT_BUTTON, self.ArchiveBtn)
        cancelBtn.Bind(wx.EVT_BUTTON, self.CancelBtn)
        
        self.SetSizer(sizer)
        self.Center()
    
    def ActionBtn(self, event):
        self.retCode = 1
        self.Show(False)
        
    def ArchiveBtn(self, event):
        self.retCode = 2
        self.Show(False)
                
    def CancelBtn(self, event):
        self.retCode = 3
        self.Show(False)