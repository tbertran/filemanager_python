#!/usr/bin/python

# dragdrop.py

from SshConnect import SshConnect

import time
#from threading import Thread
import threading
import sys
#import wx
from wx import wx #@UnresolvedImport
from ObjectListView import ObjectListView, ColumnDefn, Filter
from ActionWindow import ActionWindow
from turtle import _Root

#ID_BUTTON=100

_ROOT_DIR = '/opt/ccs'
MASTER_JOB_BTN_LABEL = 'Run ccs-master'
INC_FILE_INTERVAL = 6
mainSsh = SshConnect
filePath = ""

class MyFrame(wx.Frame):
    list_itemid = []
    flip = -1
    fileListOnSelect = str()
    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title, wx.DefaultPosition, wx.Size(1149, 658))
        self.Init()
        
    def Init(self):
        global mainSsh
        #password = wx.GetPasswordFromUser("Please enter the password for ccs:")
        password = 'maoth77'
        if not password:
            print 'exiting'
            sys.exit()
        
        mainSsh = SshConnect(password)
        mainSsh.start()
        time.sleep(1)
        
        # Make the secondary SSH connection to populate the file detail
        self.secSsh = SshConnect(password)
        self.secSsh.start()
        
        mainSsh.join()
        
        # Building Splitters
        self.splitterMain = wx.SplitterWindow(self, -1, style=wx.SP_3DSASH | wx.SP_LIVE_UPDATE)
        self.splitter3 = wx.SplitterWindow(self.splitterMain, -1, style=wx.SP_3DSASH | wx.SP_LIVE_UPDATE)
        self.splitter2 = wx.SplitterWindow(self.splitter3, -1, style=wx.SP_3DSASH | wx.SP_LIVE_UPDATE)
        self.splitter1 = wx.SplitterWindow(self.splitter2, -1, style=wx.SP_3DSASH | wx.SP_LIVE_UPDATE)
        
        
        # Building Window Components
        self.dir = wx.TreeCtrl(self.splitterMain, 1, wx.DefaultPosition,(-1, -1), wx.TR_HIDE_ROOT|wx.TR_HAS_BUTTONS)
        self.entry = wx.TextCtrl(self.splitter1,-1,size=wx.Size(40,22))
        self.fileList = MyListCtrl(self.splitter1, -1) #, style=wx.LC_LIST)
        
        
        self.miniWindow = wx.Window(self.splitter2, -1)
        self.fileDetail = wx.TextCtrl(self.splitter3, -1, style = wx.TE_READONLY | wx.TE_MULTILINE | wx.TE_PROCESS_ENTER)
        self.fileDetail.SetFont(wx.Font(14, wx.TELETYPE, wx.NORMAL, wx.LIGHT, False, u'Courier New'))
        self.checkbox = wx.CheckBox(self.miniWindow, -1, 'Auto Refresh', pos=(0,0)) #,size=(100, 100))

        # Building Events
        self.entry.Bind(wx.EVT_KEY_UP,self.OnEnteringTextInFilter)
        self.dir.Bind(wx.EVT_TREE_SEL_CHANGED, self.DirOnSelect, id=1)
        wx.EVT_LIST_ITEM_SELECTED(self, self.fileList.GetId(), self.fileListOnSelect)
        wx.EVT_CHECKBOX(self, self.checkbox.GetId(), self.ChkBoxEvt)
        wx.EVT_SPLITTER_SASH_POS_CHANGED(self, self.splitter3.GetId(), self.AdjustSplitter3SashPosition)
        wx.EVT_SPLITTER_SASH_POS_CHANGED(self, self.splitter1.GetId(), self.AdjustSplitter1SashPosition)
        wx.EVT_SPLITTER_SASH_POS_CHANGED(self, self.splitter2.GetId(), self.AdjustSplitter3SashPosition)
        wx.EVT_SPLITTER_SASH_POS_CHANGING(self, self.splitter2.GetId(), self.AdjustSplitter3SashPosition)
        wx.EVT_SPLITTER_SASH_POS_CHANGING(self, self.splitter3.GetId(), self.AdjustSplitter2SashPosition)
        #wx.EVT_ENTER_WINDOW(self, self.miniWindow.GetId(), self.MouseOnMiniWindow)
        self.miniWindow.Bind(wx.EVT_ENTER_WINDOW, self.MouseOnMiniWindow)
        self.fileList.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnDoubleClick)
        #self.Bind(wx.EVT_LIST_COL_CLICK, self.OnColClick, self.fileList)
        self.Bind(wx.EVT_CLOSE, self.OnExit)
                
        # Assembling Splitters
        self.splitterMain.SplitVertically(self.dir, self.splitter3)
        self.splitterMain.SetSashPosition(188)
        self.splitter1.SplitHorizontally(self.entry, self.fileList)
        self.splitter1.SetSashPosition(22)        
        self.splitter2.SplitHorizontally(self.splitter1, self.miniWindow)
        self.splitter2.SetSashPosition(210)
        self.splitter3.SplitHorizontally(self.splitter2, self.fileDetail)
        self.splitter3.SetSashPosition(236)
        

        # Building Top Sizer, Events
        self.masterButton = wx.Button(self, 1, MASTER_JOB_BTN_LABEL)
        self.masterButton.Bind(wx.EVT_BUTTON, self.runCcsMaster)
        self.refreshButton = wx.Button(self, 1, 'Refresh')
        self.refreshButton.Bind(wx.EVT_BUTTON, self.DirOnSelect)
        self.topSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.topSizer.Add(self.masterButton, 1) 
        self.topSizer.Add(self.refreshButton, 1) 


        # Building Main Sizer
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.mainSizer.Add(self.topSizer,0,wx.EXPAND)
        self.mainSizer.Add(self.splitterMain,1,wx.EXPAND)
        
        self.SetSizer(self.mainSizer)
        self.Centre()
        
        
        if not mainSsh.isConnectionOk():
            wx.MessageBox(caption="Password is invalid", message="Please try again.", style=wx.ICON_ERROR)
            sys.exit()
            
        self.PopulateTree(_ROOT_DIR)

        # Start the timer to check for incoming files
        incFilesThread = threading.Thread(target=self.CheckForIncomingFiles)
        incFilesThread.start()
        
    """
        EVENTS
    """
    
    def OnExit(self,event):
        mainSsh.ZombieConnection()
        time.sleep(0.5)
        self.Destroy()
        
    def DirOnSelect(self, event):
        # no directory was selected
        if not self.dir.GetItemText(self.dir.GetSelection()) == _ROOT_DIR:
            global filePath
            filePath = self.ReconstructFilePath()
            self.entry.SetValue("")
            self.Refresh() 
            self.AdjustCheckbox()
        
    def OnEnteringTextInFilter(self, event):
        self.fileList.ApplyFilter(self.entry.GetValue())
        
    def AdjustSplitter1SashPosition(self, event):  
        self.splitter1.SetSashPosition(22)
                     
    def AdjustSplitter3SashPosition(self, event):  
        self.splitter3.SetSashPosition(self.splitter2.GetSashPosition() + 26)

    def AdjustSplitter2SashPosition(self, event):  
        self.splitter2.SetSashPosition(self.splitter3.GetSashPosition() - 26)
        
    def MouseOnMiniWindow(self, event):
        pass
    
    def OnDoubleClick(self, event):
        if (filePath.split('/')[-1] == 'input' or filePath.split('/')[-1] == 'informatica_out') \
            and filePath.split('/')[-2] == _ROOT_DIR.split('/')[-1] :  
            if self.fileList.GetItem(event.GetIndex()).GetText() == 'READY':
                actionWindow = ActionWindow(self, -1, '', 'Put on Hold')
            else:
                actionWindow = ActionWindow(self, -1, '', 'Remove Hold')
            actionWindow.ShowModal()
            
            if actionWindow.retCode == 1:
                mainSsh.toggleStatus(filePath, self.selected_file)
            elif actionWindow.retCode == 2:
                mainSsh.archiveFile(filePath, self.selected_file, _ROOT_DIR)
            else:
                pass
                
            actionWindow.Destroy()
              
            #mainSsh.toggleStatus(filePath, self.selected_file)
            self.Refresh()

            
    def runCcsMaster(self, event):
        
        """
        print 'sashM ', self.splitterMain.GetSashPosition()
        print 'sash1 ', self.splitter1.GetSashPosition()
        print 'sash2 ', self.splitter2.GetSashPosition()
        print 'sash3 ', self.splitter3.GetSashPosition()
        print 'col0  ', self.fileList.GetColumnWidth(0)
        print 'col1  ', self.fileList.GetColumnWidth(1)
        print 'col2  ', self.fileList.GetColumnWidth(2)
        print 'col3  ', self.fileList.GetColumnWidth(3)
        print 'width ', self.GetSize()
        return
        """
        if wx.MessageBox(caption="The following files will be processed. Are you sure?", message=self.inputFileList, \
                         style=wx.wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION) == wx.YES:
            mainSsh.runCcsMaster(_ROOT_DIR)
        
        
        
    def PopulateTree(self, in_dir):
        if mainSsh.isConnectionOk():
            self.list = mainSsh.readDir(in_dir)
            print "self.list be4: ", self.list
            if len(self.list) == 0:
                wx.MessageBox(caption="The directory is invalid:", message=in_dir, style=wx.ICON_ERROR)
                sys.exit()
            #list = ['ccs', 'archive', 'ccs', 'archive', 'informatica_out', 'ccs', 'archive', 'informatica_input', 'ccs', 'archive', 'input', 'ccs', 'others', 'ccs', 'informatica_out', 'ccs', 'informatica_input', 'ccs', 'informatica_logs', 'ccs', 'processing', 'ccs', 'processing', 'informatica_input', 'ccs', 'processing', 'input', 'ccs', 'conf', 'ccs', 'bin']
            
            # get the list of the parent directories that we do NOT want to see in tree
            # e.g.: if _ROOT_DIR = '/opt/this/that/ccs', we only want ccs/ and its children 
            root_parents = in_dir.split("/")[1:-1]
            for dir in root_parents:
                while dir in self.list:
                    self.list.remove(dir)
                    
            # only retain the last directory in the path. In our example: ccs/
            in_dir = in_dir.split("/")[-1] 
            
            self.list_itemid = [self.dir.AddRoot(in_dir)]
            list_string = [in_dir]
    
            print "self.list during: ", self.list
            for directory in self.list: 
                if directory == in_dir.replace("/",""): # root
                    prec_dir = directory
                    continue
                do_continue = False
                for idx in range(len(list_string)):
                    if list_string[idx] == directory and \
                        self.dir.GetItemText(self.dir.GetItemParent(self.list_itemid[idx])) == prec_dir:
                        do_continue = True
                        break
                if do_continue:
                    prec_dir = directory
                    continue
                """
                if list_string.count(directory) > 0 \
                and self.dir.GetItemText(self.dir.GetItemParent(self.list_itemid[list_string.index(directory)])) == prec_dir:
                    prec_dir = directory
                    continue
                """ 
                list_string.append(directory) 
                self.list_itemid.append(self.dir.AppendItem(self.list_itemid[list_string.index(prec_dir)], directory))
                prec_dir = directory
            print "self.list after: ", list_string
            self.dir.SortChildren(self.list_itemid[0])
                
                

    
    def ReconstructFilePath(self):
        directory_item = self.dir.GetSelection()
        directory_str = self.dir.GetItemText(directory_item)
        
        # reconstruct path to directory
        path = directory_str
        parent_dir_item = self.dir.GetItemParent(directory_item)
        parent_dir_str = self.dir.GetItemText(parent_dir_item)        
        
        # iterate, working your way up the tree to the root directory
        while True:
            path = parent_dir_str + "/" + path            
            if parent_dir_item == self.list_itemid[0]: #_ROOT_DIR:
                break
            
            parent_dir_item = self.dir.GetItemParent(parent_dir_item)
            parent_dir_str = self.dir.GetItemText(parent_dir_item)
        return path
            
            
    def fileListOnSelect(self,event):
        if event == "":
            self.fileDetail.ChangeValue(self.secSsh.getFileContent(filePath, self.selected_file))
        else:
            self.fileDetail.ChangeValue(self.secSsh.getFileContent(filePath, self.getFileName(event.GetIndex())))
            self.selected_file = self.getFileName(event.GetIndex())
        self.AdjustCheckbox()

    def getFileName(self, index):
        if self.fileList.GetItem(index, 0).GetText() == "READY":
            return self.fileList.GetItem(index, 1).GetText()
        else:
            return 'hold_' + self.fileList.GetItem(index, 1).GetText()
                
    def AdjustCheckbox(self):
        for i in range(self.fileList.GetItemCount()):
            if self.fileList.GetItemState(i, wx.LIST_STATE_SELECTED): # == wx.LIST_STATE_SELECTED:
                self.checkbox.Show(True)
                return
            
        self.checkbox.SetValue(False)
        self.checkbox.Show(False)

    

    def Refresh(self): #, filter): 
        self.fileList.DoRefresh()
        
    def ChkBoxEvt(self, event):
        if self.checkbox.IsChecked():
            logsTailThread = threading.Thread(target= self.AutoRefreshFileDetail)
            logsTailThread.start()

    def CheckForIncomingFiles(self):
        while True:
            list = mainSsh.readFiles(_ROOT_DIR + '/input')
            list.extend(mainSsh.readFiles(_ROOT_DIR + '/informatica_out'))
            
            # put together list of files in input/ and informatica_out
            self.inputFileList = str()
            for file in list: 
                self.inputFileList = self.inputFileList + '\n' + file.name
            self.inputFileList = self.inputFileList[1:]
            
            if len(list) == 0:
                self.masterButton.SetLabel('no file to process')
                self.masterButton.Disable()
            else:
                if not self.masterButton.IsEnabled():
                    wx.MessageBox(caption="One or more files have just arrived!\nThe following files are present in input/, informatica_out/:", \
                                  message=self.inputFileList,\
                                   style=wx.ICON_INFORMATION)
                    
                self.masterButton.Enable()
                self.masterButton.SetLabel(MASTER_JOB_BTN_LABEL)
            
            counter = 0
            while counter < INC_FILE_INTERVAL:
                if not mainSsh.isConnectionOk():
                    return
                time.sleep(0.5)
                counter += 0.5

        
    def AutoRefreshFileDetail(self):
        while self.checkbox.IsChecked():
            self.fileListOnSelect("")
            self.fileDetail.SetSelection(self.fileDetail.GetLastPosition(), to = self.fileDetail.GetLastPosition())
            time.sleep(2) 
    
           
class MyListCtrl(ObjectListView): 
    def __init__(self, parent, id):
        ObjectListView.__init__(self, parent, id, style=wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self.Init()

    def Init(self):
        self.InitModel()
        self.InitObjectListView()

    def InitModel(self):
        self.files = mainSsh.readFiles(filePath)
    
    def DoRefresh(self):
        self.InitModel()
        if len(self.files) == 0:
            self.DeleteAllItems()
        else:
            self.SetObjects(self.files)  
    
    def InitObjectListView(self):
        self.SetColumns([
            ColumnDefn("Status", "left", 68, "status"),
            ColumnDefn("Name", "left", 594, "name"),
            ColumnDefn("Size", "left", 75, "size"),
            ColumnDefn("Modified", "left", 216, "modified"),
        ])
        self.SetObjects(self.files)   
        
        self.myFilter = Filter.TextSearch(self)   
        self.SetFilter(self.myFilter)    
        self.rowFormatter = self.rowFormatter2
        self.SetSortColumn(1) # file name

    def ApplyFilter(self, myText):
        self.myFilter.SetText(myText)
        self.RepopulateList()

    def rowFormatter2(self, listItem, lineItem):
        listItem.SetTextColour(lineItem.colour)
        listItem.SetFont(lineItem.font)        
        
        
class MyApp(wx.App):
    def OnInit(self):
        frame = MyFrame(None, -1, "File Manager")
        frame.Center()
        frame.Show(True)
        self.SetTopWindow(frame)
        return True

app = MyApp(0)
app.MainLoop()