'''
Created on Oct 8, 2010

@author: thomas
'''
from wx import wx

_READY_COLOR = '#009D4E'
_HOLD_COLOR = wx.RED

class LineItem(object):


    def __init__(self, name, size, modified):
        self.name = name
        self.size = size
        self.modified = modified
        
        if self.name.find('hold_') >= 0:
            self.status = "HOLD"
            self.name = self.name.replace("hold_", "")
            self.font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_SLANT, wx.LIGHT, False)
            self.colour = _HOLD_COLOR
        else:
            self.status = "READY"
            self.font = wx.Font(14, wx.SWISS, wx.NORMAL, wx.BOLD, False)
            self.colour = _READY_COLOR