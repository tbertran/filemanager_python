# This connects to the openbsd ftp site and
# downloads the recursive directory listing.
import pexpect
import sys
from threading import Thread
from LineItem import LineItem

class SshConnect(Thread):
    _connection_ok = True
    COMMAND_PROMPT = '[#$] ' ### This is way too simple for industrial use -- we will change is ASAP.
    def __init__(self, password):
        Thread.__init__(self)
        self.password = password
        self.SSH_NEWKEY = '(?i)are you sure you want to continue connecting'
        self.PWD_DENIED = '(?i)denied'
        self.COMMAND_PROMPT = '[#$] ' ### This is way too simple for industrial use -- we will change is ASAP.
        
    def run(self):
        #child = pexpect.spawn ('ssh thomas@leo.local')
        self.child = pexpect.spawn ('ssh -l root -p 2222 leo.local')
        i = self.child.expect([pexpect.TIMEOUT, self.SSH_NEWKEY, self.COMMAND_PROMPT, '(?i)password'])
        if i == 0: # Timeout
            print "time out error!!!"
            sys.exit()
        elif i == 1:
            self.child.sendline('yes')
            self.child.expect('(?i)to the list of known hosts.')
        elif i == 2:
            print "simple, no pwd!!!"
            #sys.exit()
            
        print "all good"

        self.child.sendline (self.password)
        
        i = self.child.expect(['(?i)denied(?i)', self.COMMAND_PROMPT])
        if i == 0:
            self._connection_ok = False
            sys.exit()
            
            
        self.COMMAND_PROMPT = "\[PEXPECT\][#$] "
        self.child.sendline ("PS1='[PEXPECT]\$ '") # In case of sh-style
        self.child.expect(self.COMMAND_PROMPT)
        self.child.sendline('cd /')
        self.child.expect(self.COMMAND_PROMPT)

    def readDir(self, dir):
        mycomm = 'find -L ' + dir + ' -type d'
        self.child.sendline (mycomm)
        self.child.expect(self.COMMAND_PROMPT)
        feed = self.child.before.replace(mycomm,"").replace("\n","").replace("\r","").split('/')
        for item in feed:
            if item.find("No such file") >= 0:
                #print 'this is it!'
                feed = ""
                return feed
        return feed[2:]
        
    def readFiles(self, dir):
        if not dir:
            return
         
        mycomm = 'find -L ' + dir + '/* -prune -type f -exec stat --format=%n\"^\"%s\"^\"%y {} \;'
        self.child.sendline (mycomm)
        self.child.expect(self.COMMAND_PROMPT)       
        feed  = self.child.before 
        if feed.find("No such file or directory") >= 0:
            #feed = ""
            return []
        
        feed = feed.replace("\r","").replace("\n",";").split(';')
    
        i = 0
        feed_aux = feed
        for info in feed_aux[2:-1]:
            if info.find('nohup') >= 0:
                continue
            
            info = info.split('^')
            
            # remove milliseconds in date
            temp_str = info[2].split('.')
            info[2] = temp_str[0]
            
            # only retain file name (equivalent of basename)
            temp_str = info[0].split('/')
            info[0] = temp_str[len(temp_str) - 1] # last element in list
                            
            feed[i] = LineItem(info[0], info[1], info[2])
            i = i + 1

        return feed[:i]        
        
    def getFileContent(self,dir_path, file_name):
        file_path = dir_path + "/" + file_name
        mycomm = 'cat ' + file_path + ' 2> /dev/null'
        self.child.sendline (mycomm)
        self.child.expect(self.COMMAND_PROMPT)        
        return self.child.before.replace(mycomm + "\r\n", "").replace("\r", "")
    
    def runCcsMaster(self,ROOT_DIR):
        print 'running ccs-master'
        self.child.sendline ("nohup " + ROOT_DIR + "/bin/ccs-master.sh > /dev/null 2>&1 &")
        self.child.expect(self.COMMAND_PROMPT)
        
    def isConnectionOk(self):
        return self._connection_ok
    
    def ZombieConnection(self):
        self._connection_ok = False
    
    def toggleStatus(self, dir_path, file_name):
        if file_name.find("hold_") >= 0:
            mycomm = 'mv ' + dir_path + "/" + file_name + ' ' + dir_path + "/" + file_name.replace('hold_', '')
        else:
            mycomm = 'mv ' + dir_path + "/" + file_name + ' ' + dir_path + "/hold_" + file_name
        
        self.child.sendline (mycomm)
        self.child.expect(self.COMMAND_PROMPT)
        
    def archiveFile(self, dir_path, file_name, root_dir):
        mycomm = 'mv ' + dir_path + "/" + file_name + ' ' \
            + root_dir + "/archive/" + dir_path.split("/")[-1]
        self.child.sendline (mycomm)
        self.child.expect(self.COMMAND_PROMPT)