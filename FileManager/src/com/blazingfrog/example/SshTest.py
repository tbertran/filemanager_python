#import pxssh
from pxssh import ExceptionPxssh
from pxssh import pxssh
import getpass
try:                                                            
    s = pxssh()
    hostname = 'appletv.local' #raw_input('hostname: ')
    username = 'frontrow' #raw_input('username: ')
    password = 'frontrow' #getpass.getpass('password: ')
    s.login (hostname, username, password) 
    s.prompt()             # match the prompt
    print s.before         # print everything before the prompt.
    s.sendline ('ls -l')
    s.prompt()
    print s.before
    s.sendline ('df')
    s.prompt()
    print s.before
    s.logout()
except ExceptionPxssh, e:
    print "pxssh failed on login." 
    print str(e)
