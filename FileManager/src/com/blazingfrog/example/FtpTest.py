# This connects to the openbsd ftp site and
# downloads the recursive directory listing.
import pexpect
child = pexpect.spawn ('ftp ftp.openbsd.org')
child.expect ('Name .*: ')
child.sendline ('anonymous')
child.expect ('Password:')
child.sendline ('noah@example.com')
child.expect ('ftp> ')
child.sendline ('cd pub')
child.expect('ftp> ')
child.sendline ('cd OpenBSD')
child.expect('ftp> ')
#child.sendline ('get README')
child.sendline ('ls -al')
child.expect('ftp> ')
print child.before
#child.sendline ('bye')
child.interact()